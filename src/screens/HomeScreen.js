import React, {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';
import styled from '@emotion/native';
import {PieChart} from 'react-native-chart-kit';
import _ from 'lodash';

// api
import {getLastInfomation} from '../api';

// util
import {vwFromPX} from '../utils';

const screenWidth = Dimensions.get('window').width;

const HomeScreen = (props) => {
  const [todayData, setTodayData] = useState();
  const [isLoad, setIsLoad] = useState(false);

  useEffect(() => {
    checkInformation();
  }, []);

  const checkInformation = async () => {
    const response = await getLastInfomation();
    console.log('data >>> ', response);
    setTodayData(response);
    setIsLoad(true);
  };

  const chartConfig = {
    backgroundColor: '#1cc910',
    backgroundGradientFrom: '#eff3ff',
    backgroundGradientTo: '#efefef',
    decimalPlaces: 2,
    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
    style: {
      borderRadius: 16,
    },
  };

  return (
    <Container>
      <TitleText size={25}>Lastest in china</TitleText>
      <TitleText size={20}>{`Infected case: ${_.get(
        todayData,
        'infected',
      )}`}</TitleText>
      <InfoView>
        {isLoad && (
          <PieChart
            data={[
              {
                name: 'Deceased',
                population: _.get(todayData, 'deceased'),
                color: '#d35d6e',
                legendFontColor: '#000',
                legendFontSize: 10,
              },
              {
                name: 'Recovered',
                population: _.get(todayData, 'recovered'),
                color: '#5aa469',
                legendFontColor: '#000',
                legendFontSize: 10,
              },
              {
                name: 'Active Cases',
                population: _.get(todayData, 'activeCases'),
                color: '#f8d49d',
                legendFontColor: '#000',
                legendFontSize: 10,
              },
            ]}
            width={screenWidth}
            height={220}
            chartConfig={chartConfig}
            accessor="population"
            backgroundColor="transparent"
            paddingLeft="15"
            absolute //for the absolute number remove if you want percentage
          />
        )}
      </InfoView>
      <TitleText size={15}>{`Number of Tested: ${_.get(
        todayData,
        'tested',
      )}`}</TitleText>
      <TitleText size={10}>{'REF: https://apify.com/covid-19'}</TitleText>
    </Container>
  );
};

const Container = styled.View`
  height: 100%;
  width: 100%;
  background: black;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TitleText = styled.Text`
  font-size: ${(props) => vwFromPX(props.size)};
  font-weight: bold;
  color: white;
  text-align: center;
  margin: 10px 10px;
`;

const ImageRefresh = styled.Image`
  height: ${vwFromPX(40)};
  width: ${vwFromPX(40)};
  margin: 20px;
`;

const InfoView = styled.View`
  display: flex;
  background: #efefef;
  border: 2px solid black;
  border-radius: 50px;
  margin: 20px;
`;

export default HomeScreen;
