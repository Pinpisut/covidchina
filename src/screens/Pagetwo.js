import React from 'react';
import styled from '@emotion/native';
import {Text} from 'react-native';

// import {
//   getWidthFromPixel,
//   getHeightFromPixel,
//   getSizeWithOutPixel,
//   getSizeWithOutPixelHeight,
// } from '../utils';

const Pagetwo = (props) => {
  const {history} = props;

  return (
    <Container>
      <Text>Two</Text>
    </Container>
  );
};

const Container = styled.View`
  height: 100%;
  width: 100%;
  background: red;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Pagetwo;
