import {lazy} from 'react';

const HomeScreen = lazy(() => import('../../screens/HomeScreen.js'));
const Pagetwo = lazy(() => import('../../screens/Pagetwo'));

export default [
  {
    page: 'HomeScreen',
    menu: 'Home',
    to: '/',
    exact: true,
    component: HomeScreen,
    visible: true,
  },
  {
    page: 'Pagetwo',
    menu: 'pagetwo',
    to: '/pagetwo',
    exact: true,
    component: Pagetwo,
    visible: true,
  },
];
