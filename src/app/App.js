/* eslint-disable react-native/no-inline-styles */

// create by Pinpisut Sengcha for covid-china

import posed from 'react-native-pose';
import React, {useState, useEffect, Suspense} from 'react';
import {Dimensions, Text, View} from 'react-native';
import styled from '@emotion/native';
import _ from 'lodash';
import {NativeRouter, Route, Link} from 'react-router-native';

import routes from '../app/routes';

const {width: screenWidth, height: screenHeight} = Dimensions.get('window');

export default function App() {
  const [nowPage, setPage] = useState('home');

  const renderPage = () => {
    const pages = _.map(routes, (route, key) => {
      return (
        <Route
          exact={route.exact}
          path={route.to}
          component={route.component}
          key={key}
        />
      );
    });
    return pages;
  };

  return (
    <Container>
      <Suspense
        fallback={
          <View>
            <Text>Loading...</Text>
          </View>
        }>
        <NativeRouter>
          <RouteContainer
            initalPose="exit"
            pose="enter"
            key={nowPage}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              zIndex: 999,
              width: '100%',
              height: '100%',
            }}>
            <WrapPage>
              <Page>{renderPage()}</Page>
            </WrapPage>
          </RouteContainer>
        </NativeRouter>
      </Suspense>
    </Container>
  );
}

const Container = styled.View`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: white;
  width: ${screenWidth}px;
  height: ${screenHeight}px;
`;

const WrapPage = styled.View`
  display: flex;
  flex: 1;
  width: 100%;
  height: 100%;
`;

const Page = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
`;

const RouteContainer = posed.View({
  enter: {
    opacity: 1,
    delay: 2 * 1000,
    transition: {
      duration: 1000,
    },
    // beforeChildren: true
  },
  exit: {
    opacity: 0,
    transition: {
      duration: 0,
    },
  },
});
