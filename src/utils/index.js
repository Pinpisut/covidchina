import {Dimensions, StatusBar} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const designWidth = 320;
const designHeight = 568;

const vw = (number) => {
  const unit = 'px';
  const result = Math.round((number * windowWidth) / 100);
  return `${result}${unit}`;
};
const vh = (number) => {
  const unit = 'px';
  const result = Math.round((number * windowHeight) / 100);
  return `${result}${unit}`;
};

const vwFromPX = (number) => {
  const result = Math.round((number * 100) / designWidth);
  return vw(result);
};

const vhFromPX = (number) => {
  const result = Math.round((number * 100) / designHeight);
  return vh(result);
};

const contentHeight = windowHeight - StatusBar.currentHeight;

export {vw, vh, vwFromPX, vhFromPX, windowWidth, windowHeight, contentHeight};
