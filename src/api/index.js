import axios from 'axios';

// const baseURL = 'https://api.covid19api.com';

const api = axios.create({});

const API = ({url = ''}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response;
      const urlAndParam = `${url}`;
      response = await api.get(urlAndParam);
      console.log('api response >>', response.data);
      resolve(response.data);
    } catch (error) {
      console.log('fetch error >>>', {url, error: error});
      console.log('see more >>>> ', error.response.data);
    }
  });
};

export const getLastInfomation = () => {
  const setting = {
    url:
      'https://api.apify.com/v2/key-value-stores/x4iHxk7TVGI7UxFv6/records/LATEST?disableRedirect=true',
  };
  const response = API(setting);
  return response;
};

export const getCaseOnState = () => {
  const setting = {
    url: 'https://api.covid19api.com/dayone/country/china/status/confirmed',
  };

  const response = API(setting);
  return response;
};
